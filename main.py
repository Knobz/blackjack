import random

class Blackjack():
    def __init__(self, deck_count, players):
        self.deck_count = deck_count
        self.players = players
        self.player_hands = []
        self.hidden_card = 0
        self.deck = self.create_deck(self.deck_count)
        
        
    def create_deck(self, deck_count):
        deck = []
        suits = ["A", "J", "Q", "K"]
        for i in range(deck_count):
            for j in range(4):
                for k in range(2,11):
                    deck.append(k)
                for l in suits:
                    deck.append(l)
        random.shuffle(deck)
        return deck
    
    def deal_cards(self):
        for i in range(self.players + 1):
            self.player_hands.append([])
        for i in range(2):
            for j in range(self.players+1):
                self.player_hands[j].append(self.deck[0])
                self.deck.pop(0)
        self.hidden_card = self.player_hands[-1][-1]
        self.player_hands[-1].pop(-1)

if __name__ == "__main__":
    b = Blackjack(1, 3)
    b.deal_cards()
    print(b.deck)
    print(b.player_hands)